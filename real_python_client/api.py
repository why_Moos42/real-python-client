import requests
import json
from time import sleep

class ReqresClient:
    url = 'https://reqres.in/api/'

    def __init__(self):
        pass

    def do_something(self):
        count = 0
        for test in req.test_cases:
            if (test[0:4] == '{\n "') and (not test[0:9] == '{\n "error') or (test[0:1] == ''):
                count += 1

        if (count > 0):
            print(count)
            return 'done'

            # while (test == True):
            #     print(test)
            #     if (test == False): continue
            #     return 'done'

        # test_case_success = []
        #
        # for test in test_cases:
        #     if (test == 200) or (test == 201) or (test == 204):
        #         test_case_success.append(test)
        #     elif (test == 400) or (test == 404):
        #         continue
        #
        # if (len(test_case_success) >= 1):
        #     return 'done'

    def do_something_failure(self):
        count = 0
        for test in req.test_cases:
            if (test[0:9] == '{\n "error') or (test[0:2] == '{}'):
                count += 1

        if (count > 0):
            print(count)
            return 'not done'

            # while (test == False):
            #     print(test)
            #     if (test == True): continue
            #     return 'not done'

        # for test in test_cases:
        #     if (test == 200) or (test == 201) or (test == 204):
        #         continue
        #     elif (test == 400) or (test == 404):
        #         test_case_failures.append(test)
        #
        # if (len(test_case_failures) >= 1):
        #     return 'not done'

    def list_users(self, page):
        # print(f'GET: LIST USERS - page={page}')
        response = requests.get(self.url + 'users', params={'page':page})
        data = json.dumps(response.json(), indent=1)
        return data

    def single_user(self, id):
        # print(f'GET: SINGLE USER - id={id}')
        response = requests.get(self.url + f'users/{id}')
        data = json.dumps(response.json(), indent=1)
        return data

    def list_resources(self):
        # print('GET: LIST RESOURCES')
        response = requests.get(self.url + 'unknown')
        data = json.dumps(response.json(), indent=1)
        return data

    def single_resource(self, id):
        # print(f'GET: SINGLE RESOURCE - id={id}')
        response = requests.get(self.url + f'unknown/{id}')
        data = json.dumps(response.json(), indent=1)
        return data

    def create_user(self, name, job):
        # print('POST: CREATE USER')
        data = {
            'name': name,
            'job': job
        }
        response = requests.post(self.url + 'users', data=data)
        data = json.dumps(response.json(), indent=1)
        return data

    def update_user(self, id, name, job):
        # print(f'PUT: UPDATE USER - id={id}')
        data = {
            'name': name,
            'job': job
        }
        response = requests.put(self.url + f'users/{id}', data=data)
        data = json.dumps(response.json(), indent=1)
        return data

    def update_user_patch(self, id, name, job):
        # print(f'PATCH: UPDATE USER - id={id}')
        data = {
            'name': name,
            'job': job
        }
        response = requests.patch(self.url + f'users/{id}', data=data)
        data = json.dumps(response.json(), indent=1)
        return data

    def delete_user(self, id):
        # print(f'DELETE USER - id={id}')
        response = requests.delete(self.url + f'users/{id}')
        return response.text

    def register_user(self, email, password):
        # print('POST: REGISTER USER')
        data = {
            'email': email,
            'password': password
        }
        response = requests.post(self.url + 'register', data=data)
        data = json.dumps(response.json(), indent=1)
        return data

    def login_user(self, email, password):
        # print('POST: LOGIN USER')
        data = {
            'email': email,
            'password': password
        }
        response = requests.post(self.url + 'login', data=data)
        data = json.dumps(response.json(), indent=1)
        return data

    def delay_users(self, delay):
        # print(f'GET: USERS DELAYED RESPONSE')
        response = requests.get(self.url + 'users', params={'delay': delay})
        sleep(delay)
        data = json.dumps(response.json(), indent=1)
        return data


req = ReqresClient()
req.test_cases = [
            req.list_users(page=2),
            req.single_user(id=2),
            req.single_user(id=23),
            req.list_resources(),
            req.single_resource(id=2),
            req.single_resource(id=23),
            req.create_user(name='morpheus', job='leader'),
            req.update_user(id=2, name='morpheus', job='zion resident'),
            req.update_user_patch(id=2, name='morpheus', job='zion resident'),
            req.delete_user(id=2),
            req.register_user(email='eve.holt@reqres.in', password='pistol'),
            req.register_user(email='sydney@file', password=''),
            req.login_user(email='eve.holt@reqres.in', password='cityslicka'),
            req.login_user(email='peter@klaven', password=''),
            req.delay_users(delay=3),
        ]
