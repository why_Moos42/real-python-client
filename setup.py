# Learn how to create a setup file
# Similarly how java ships as a .jar file , python ships either as a .tar.gz file or .whl file

import setuptools

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setuptools.setup(
    name='real_python_client',
    # ignore test files for packaging
    packages=setuptools.find_packages(exclude=['t', 't.*']),
    version= '1.0.0',
    description='real_python_client',
    author='Yusuf Moosa',
    author_email='yusufmusa123@gmail.com@email.com',
    url='git@bitbucket.org:why_Moos42/real-python-client.git',
    classifiers=[],
    package_data={
    },
    install_requires=requirements
)
